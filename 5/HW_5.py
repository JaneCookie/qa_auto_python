# 1. Напишіть функцію, що приймає один аргумент. Функція має вивести на екран тип цього аргумента:
# (для визначення типу використайте type)
def bar(arg):
    print(type(arg))


bar('Hello_world')


# 2. Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
# Якщо перетворити не вдається, функція має повернути 0 (флоатовий нуль).
# def foo(arg):
#
#     if type(arg) is float:
#         print('It is a float')
#         return arg
#     elif type(arg) is int:
#         print('You entered the int')
#         change_arg = float(arg)
#         return change_arg
#     elif type(arg) in (str, bool, list, dict, set, tuple):
#         print('The change is impossible')
#         return 0.0
#
#
# result = foo('0.3')
# print(result)

def foo(arg):
    try:
        if arg is not float:
            print('it is not a float')
            return float(arg)
    except:
        print('Change is not impossible')
        return 0.0


res = foo('Hello')
print(res)

# 3. Напишіть функцію, що приймає два аргументи. Функція повинна
# - якщо аргументи відносяться до числових типів - повернути різницю цих аргументів,
# - якщо обидва аргументи це строки - обʼєднати в одну строку та повернути,
# - якщо перший строка, а другий ні - повернути dict де ключ це перший аргумент, а значення - другий,
# - у будь-якому іншому випадку повернути кортеж з цих аргументів
def foo(arg_1, arg_2):

    if type(arg_1) in (int, float) and type(arg_2) in (int, float):
        print('You entered int or float arguments. Return subtraction.')
        return arg_1 - arg_2
    elif type(arg_1) == str and type(arg_2) == str:
        print('You entered two str arguments. Return sum.')
        return arg_1 + arg_2
    elif type(arg_1) == str and type(arg_2) != str:
        my_dict = dict()
        my_dict[arg_1] = arg_2
        print('The first argument is str, and the second argument not a str. Return dict.')
        return my_dict
    else:
        print('You entered another type of arguments. Return tuple.')
        return arg_1, arg_2


result = foo('2', 1)
print(result)

