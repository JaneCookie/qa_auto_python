# 1.Дана довільна строка. Напишіть код, який знайде в ній і виведе на екран кількість слів,
# які містять дві голосні літери підряд.
import re

my_str = 'Winter holidays are always connected with presents, good mood, parties and a lot of fun.'


def find_two_vowels(my_str):
    matches = re.findall(r'[aeiouy]{2}', my_str)
    if matches:
        count = len(matches)
        print(f'There are {count} words with double vowels in this string.')
        for let in matches:
            print(let)


find_two_vowels(my_str)

# 2.Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами: { "cilpo": 47.999, "a_studio": 42.999, "momo": 49.999,
# "main-service": 37.245, "buy.ua": 38.324, "my-store": 37.166, "the_partner": 38.988, "sto": 37.720,
# "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів,
# ціни яких попадають в діапазон між мінімальною і максимальною ціною.
# Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "my-store", "main-service"

lower_limit = 36.9
upper_limit = 38.0

my_dict = {"cilpo": 47.999,
           "a_studio": 42.999,
           "momo": 49.999,
           "main-service": 37.245,
           "buy.ua": 38.324,
           "my-store": 37.166,
           "the_partner": 38.988,
           "sto": 37.720,
           "rozetka": 38.003,
           }

for key, value in my_dict.items():
    if value > lower_limit and value < upper_limit:
        print('Shop -->', key)

