def foo(user_age):
    """
    This func checks the entered user_age.
    :param user_age: int
    :return: True
    """

    len_of_number = int(len(str(user_age)) * '1')
    if user_age >= 1 and user_age <= 120:
        if user_age % len_of_number == 0 and len_of_number > 1:
            return True
    else:
        print("Oops! That isn't a valid number.  Try again...")


user_age = int(input('Enter your age: '))


def bar():
    """ This func returns a certain answer depending on the entered user_age."""

    if foo(user_age):
        print(f'Oh, you are {user_age} years old! What an interesting age!')
    elif user_age <= 0:
        print('Your age is not enough.')
    elif user_age > 120:
        print("Oops! That isn't a valid number.  Try again... ")
    elif user_age < 7:
        print(f'You are {user_age} years old! Where are your parents?')
    elif user_age < 16:
        print(f'You are only {user_age} years old, and this is an adult movie!')
    elif user_age > 65:
        print(f'Are you {user_age} years old? Show your pension sertificate!')
    else:
        print(f'Despite the fact, you are {user_age} years old, there are still no tickets!')
