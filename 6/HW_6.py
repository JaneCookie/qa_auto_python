# Hапишіть программу "Касир в кінотеатрі", яка буде виконувати наступне:
#
# Попросіть користувача ввести свій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача складається з однакових цифр (11, 22, 44 і тд років, всі можливі варіанти!) -
# вивести "О, вам <>! Який цікавий вік!"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <>, білетів все одно нема!"
#
# Замість <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача.
# Наприклад :
# "Тобі ж 5 років! Де твої батьки?"
# "Вам 81 рік? Покажіть пенсійне посвідчення!"
# "О, вам 33 роки! Який цікавий вік!"
#
# Зробіть все за допомогою функцій! Для кожної функції пропишіть докстрінг або тайпхінтінг.
# Не забувайте що кожна функція має виконувати тільки одне завдання і про правила написання коду.

def foo(user_age):
    """
    This func checks the entered user_age.
    :param user_age: int
    :return: True
    """

    len_of_number = int(len(str(user_age)) * '1')
    if user_age >= 1 and user_age <= 120:
        if user_age % len_of_number == 0 and len_of_number > 1:
            return True
    else:
        print("Oops! That wasn't a valid number.  Try again...")


user_age = int(input('Enter your age: '))


def bar():
    """ This func returns a certain answer depending on the entered user_age."""

    if foo(user_age):
        print(f'Oh, you are {user_age} years old! What an interesting age!')
    elif user_age <= 0:
        print('Your age is not enough.')
    elif user_age < 7:
        print(f'You are {user_age} years old! Where are your parents?')
    elif user_age < 16:
        print(f'You are only {user_age} years old, and this is an adult movie!')
    elif user_age > 65:
        print(f'Are you {user_age} years old? Show your pension sertificate!')
    else:
        print(f'Despite the fact, you are {user_age} years old, there are still no tickets!')


bar()
