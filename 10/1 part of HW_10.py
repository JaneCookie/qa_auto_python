# Завдання №1
# Розробити клас Людина. Людина має:
# Ім'я
# Прізвище
# Вік (атрибут, але ж змінний)
# Стать
# Люди можуть:
# Їсти
# Спати
# Говорити
# Ходити
# Стояти
# Лежати
# Також ми хочемо контролювати популяцію людства. Змінювати популяцію можемо в __init__. Треба сказати,
# що доступ до статичних полів класу з __init__ не може іти через НазваКласу.статичний_атрибут, позаяк ми не можемо
# бачити імені класу. Але натомість ми можемо сказати self.__class__.static_attribute.

import datetime


class Human:
    population = 0

    def __init__(self, name, surname, sex, birth_date=datetime.date.today()):
        self.name = name
        self.surname = surname
        self.sex = sex
        self.birth_date = birth_date
        self.__class__.increase_population()

    @classmethod
    def increase_population(cls):
        print('New human was born.')
        cls.population += 1

    @property
    def age(self):
        return (datetime.date.today() - self.birth_date).days // 365

    def eat(self, dish):
        print(f'{self.name} is eating {dish}.')

    def sleep(self):
        print(f'{self.name} sleeps.')

    def speak(self):
        print(f'{self.name} says something.')

    def walk(self):
        print(f'{self.name} {self.surname} goes for a walk.')

    def stand(self):
        print(f'{self.name} {self.surname} stands still.')

    def lie(self):
        print(f'{self.name} {self.surname} lies on the sofa.')


vasyl = Human('Vasyl', 'Petrovych', 'man', birth_date=datetime.date(1990, 5, 19))
print(vasyl.age)
mary = Human('Mary', 'Pavlivna', 'woman', birth_date=datetime.date(2000, 3, 8))
print(mary.age)
print(vasyl.eat('Fries'))
print(vasyl.sleep())
print(mary.speak())
print(vasyl.walk())
print(mary.stand())
print(vasyl.lie())
print(mary.population)
print(vasyl.population)