# Розробити функцію, котра приймає колекцію та обʼєкт функції, що приймає один аргумент.
# Повернути колекцію, кожен член якої є перетвореним членом вхідної колекції.
#
#Нотатка. Обʼєкт функції, яку передаємо вказує на функцію, котра приймає один аргумент.
# Не користуватися функціями map чи filter!!!
import math
my_collection = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


def multiple(arg: [str, int, float, list]) -> [str, int, float, list]:
    return arg * 2


def func(collection: [list, tuple], func_obj) -> list:
    return [func_obj(i) for i in collection]


print(func(my_collection, math.sqrt))

print(func(my_collection, multiple))
