# Створити клас Vehicle (транспортний засіб):
# ні від чого не наслідується
# в ініціалізатор класу (__init__ метод) передати
# producer: str
# model: str
# year: int
# tank_capacity: float # обєм паливного баку
# tank_level: float = 0 # початковий параметр рівня паливного баку дорівнює 0, параметр в аргументах не передавати
# maxspeed: int
# fuel_consumption: float # litres/100km споживання пального
# odometer_value: int = 0 # при сході з конвеєра пробіг нульовий, параметр в аргументах не передавати
#
# 1. визначити метод __repr__, яким повертати інформаційну стрічку (наповнення на ваш вибір,
# проте параметри model and year and odometer_value мають бути передані
#
# 2. написати метод refueling, який не приймає жодного аргумента, заправляє автомобіль на уявній автозаправці
# до максимума (tank_level = tank_capacity), після чого виводить на екран повідомлення, скільки літрів було заправлено
# (перша заправка буде повного баку, а в інших випадках величина буде залежати від залишку пального в баку)
#
# 3. написати метод race, який приймає один аргумент (не враховуючи self) - відстань, яку потрібно проїхати,
# а повертає словник, в якому вказано, скільки авто проїхало, з огляду на заповнення паливного баку перед поїздкою,
# залишок пального (при малому кілометражі все пальне не використається), та час, за який відбулася дана поїздка,
# з урахування, що середня швидкість складає 80% від максимальної (витрата пального рівномірна незалежно від швидкості)
# за результатами роботи метода в атрибуті tank_level екземпляра класа має зберігатися залишок пального після поїздки
# (зрозуміло, що не менше 0)
# збільшити на величину поїздки показники odometer_value
#
# 4. написати метод lend_fuel, який приймає окрім self ще й other обєкт, в результаті роботи якого паливний бак обєкта,
# на якому було викликано відповідний метод, наповнюється до максимального рівня за рахунок відповідного зменшення
# рівня пального у баку дружнього транспортного засобу
# 5. вивести на екран повідомлення з текстом типу "Дякую, бро, виручив. Ти залив мені *** літрів пального"
# у випадку, якщо бак першого обєкта повний, або у другого обєкта немає пального, вивести повідомлення
# "Нічого страшного, якось розберуся"
#
# 6. написати метод get_tank_level, для отримання інформації про залишок пального конкретного інсттанса
#
# 7. написати метод get_mileage, який поверне значення пробігу odometer_value
#
# 8. написати метод __eq__, який приймає окрім self ще й other обєкт (реалізація магічного методу для
# оператора порівняння == )
# даний метод має повернути True у випадку, якщо 2 обєкта, які порівнюються, однакові за показниками року випуску
# та пробігу (значення відповідних атрибутів однакові, моделі можуть бути різними)
#
# 9. створіть не менше 2-х обєктів класу, порівняйте їх до інших операцій, заправте їх, покатайтесь на них
# на різну відстань, перевірте пробіг, позичте один в одного пальне, знову порівняйте

class Vehicle:
    def __init__(self, producer: str, model: str, year: int, maxspeed: int, fuel_consumption: float,
                 tank_capacity: float, tank_level: float = 0.0, odometer_value: int = 0):
        self.producer = producer
        self.model = model
        self.year = year
        self.maxspeed = maxspeed
        self.fuel_consumption = fuel_consumption
        self.tank_capacity = tank_capacity
        self.tank_level = tank_level
        self.odometer_value = odometer_value

    def __repr__(self):
        return f'This vehicle is {self.producer} car, {self.model} model, {self.year} year, with {self.odometer_value} ' \
               f'odometer_value.'


    def refueling(self):
        refuel = self.tank_capacity - self.tank_level

        self.tank_level = self.tank_capacity
        print(f'Было залито {refuel} литров бензина.')


    def race(self, miles):

        max_dist = (self.tank_level / self.fuel_consumption) * 100
        if miles > max_dist:
            miles = max_dist

        self.tank_level -= (miles * self.fuel_consumption) / 100
        time = miles / (self.maxspeed * 0.8)

        self.odometer_value += miles
        data = {
            'time': time,
            'miles': miles,
            'tank_level': self.tank_level,
        }
        return data


    def lend_fuel(self, other):
        fuel_need = self.tank_capacity - self.tank_level
        fuel_capability = 0

        if self.tank_level == self.tank_capacity or other.tank_level == 0:
            print('Нічого страшного, якось розберуся')
            return

        if other.tank_level < fuel_need:
            fuel_capability = other.tank_level
        elif other.tank_level > fuel_need:
            fuel_capability = fuel_need

        self.tank_level += fuel_capability
        other.tank_level -= fuel_capability
        print(f'Дякую, бро, виручив. Ти залив мені {fuel_capability} літрів пального')


    def get_tank_level(self):
         return self.tank_level



    def get_mileage(self):
        return self.odometer_value


    def __eq__(self, other):
        return self.year == other.year and self.odometer_value == other.odometer_value


if __name__ == '__main__':
    car_1 = Vehicle('Volkswagen', 'Touareg', 2022, 100, 10.0, 60.0)
    car_2 = Vehicle('Mercedes-Benz', 'GLE 300', 2022, 100, 12.0, 60.0)
    print(car_2 == car_1)
    car_1.refueling()
    car_1.race(80)
    car_1.lend_fuel(car_2)
    car_2.lend_fuel(car_1)
    car_1.get_mileage()
    print(car_2 == car_1)