
# Задача 1: Створіть дві змінні first=10, second=30.
# Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.
first = 10
second = 30

result_sum = first + second
print(result_sum)

result_sub = first - second
print(result_sub)

result_mult = first * second
print(result_mult)

result_div = first / second
print(result_div)

result_expo = first ** second
print(result_expo)

result_whole_part = first // second
print(result_whole_part)

result_fract_part = first % second
print(result_fract_part)


# Задача 2: Створіть змінну и по черзі запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1.
# Виведіть на екран результат кожного порівняння.

result_of_inequality = first < second
print('first < second ==> ', first < second)

result_of_inequality = first > second
print('first > second ==> ', first > second)

result_affirmation_of_equality = first == second
print('first == second ==> ', first == second)

result_affirmation_of_inequality = first != second
print('first != second ==> ', first != second)



# Задача 3: Створіть змінну - результат конкатенації (складання) строк str1="Hello " и str2="world". Виведіть на екран.

str_1 = 'Hello'
str_2 = '_world'
print(str_1 + str_2)
