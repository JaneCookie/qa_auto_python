# 1.Cформуйте строку, яка містить певну інформацію про символ в відомому слові.
# Наприклад "The [номер символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
# Слово та номер отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python", а номер символу - 3) - "The 3 symbol in "Python" is 't' ".

word = 'Attention'
symbol_num = 5
print(f'The {symbol_num} symbol in {word} is {word[4]}.')




# 2.Ввести з консолі строку зі слів (або скористайтеся константою).
# Напишіть код, який визначить кількість слів в цих даних.

my_str = input("Enter words:  ")
result = len(my_str.split()) and len(my_str.split(','))
print("There are {} words.".format(str(result)))




# 3.Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть механізм, який сформує новий list (наприклад lst2), який би містив всі числові змінні, які є в lst1.
# Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

list_1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
list_2 = []
for elem in list_1:
    if type(elem) in (int, float):
        list_2.append(elem)
print('list_1 = {} \nlist_2 = {}'.format(list_1, list_2))
